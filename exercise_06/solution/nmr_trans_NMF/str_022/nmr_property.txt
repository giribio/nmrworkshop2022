-------------------------------------------------------------
----------------------- !PROPERTIES! ------------------------
-------------------------------------------------------------
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 1
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 1
   prop. index: 1
        SCF Energy:     -209.1784723453
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 1
   prop. index: 1
   Number of Alpha Electrons                 16.0000093471 
   Number of Beta  Electrons                 16.0000093471 
   Total number of  Electrons                32.0000186942 
   Exchange energy                          -21.4716857348 
   Correlation energy                        -1.2749991933 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy              -22.7466849281 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)    -209.1784723453 
# -----------------------------------------------------------
$ Mayer_Pop
   description: The Mayer population analysis
   geom. index: 1
   prop. index: 1
     Number of atoms                     : 9
     Threshold for printing              : 0.1000000
     Number of bond orders printed       : 8
  NA   - Mulliken gross atomic population
  ZA   - Total nuclear charge
  QA   - Mulliken gross atomic charge
  VA   - Mayer's total valence
  BVA  - Mayer's bonded valence
  FA   - Mayer's free valence
  ATOM       NA         ZA         QA         VA         BVA        FA
  0   0     5.8302     6.0000     0.1698     4.2225     4.2225    -0.0000
  1   0     8.3575     8.0000    -0.3575     2.1368     2.1368     0.0000
  2   0     7.1911     7.0000    -0.1911     3.0475     3.0475    -0.0000
  3   0     6.1770     6.0000    -0.1770     3.8673     3.8673     0.0000
  4   0     0.9395     1.0000     0.0605     1.0025     1.0025    -0.0000
  5   0     0.8493     1.0000     0.1507     1.0215     1.0215     0.0000
  6   0     0.8718     1.0000     0.1282     0.9991     0.9991    -0.0000
  7   0     0.8966     1.0000     0.1034     0.9907     0.9907     0.0000
  8   0     0.8869     1.0000     0.1131     0.9829     0.9829     0.0000
      Bond orders larger than 0.1000000
           Atom A     A.N. of A          Atom B    A.N. of B              Bond order
                0             6               1            8                2.007047
                0             6               2            7                1.199569
                0             6               4            1                0.984476
                2             7               3            6                0.897360
                2             7               5            1                0.974375
                3             6               6            1                0.969080
                3             6               7            1                0.978082
                3             6               8            1                0.975436
# -----------------------------------------------------------
$ Calculation_Info
   description: Details of the calculation
   geom. index: 1
   prop. index: 1
     Multiplicity:                          1
     Charge:                                0
     number of atoms:                       9
     number of electrons:                   0
     number of frozen core electrons:       1
     number of correlated electrons:        0
     number of basis functions:             194
     number of aux C basis functions:       0
     number of aux J basis functions:       251
     number of aux JK basis functions:      0
     number of aux CABS basis functions:    0
     Total Energy                           -209.178472
# -----------------------------------------------------------
$ SCF_Electric_Properties
   description: The SCF Calculated Electric Properties
   geom. index: 1
   prop. index: 1
       Filename                          : nmr.scfp 
       Do Dipole Moment Calculation      : true 
       Do Quadrupole Moment Calculation  : false 
       Do Polarizability Calculation     : false 
** Dipole moment part of electric properties **
        Magnitude of dipole moment (Debye) :        4.0056563727
        Electronic Contribution:
                  0    
      0      -2.419774
      1       0.840150
      2      -2.055232
        Nuclear Contribution:
                  0    
      0       2.975928
      1      -2.205731
      2       2.611459
        Total Dipole moment:
                  0    
      0       0.556154
      1      -1.365581
      2       0.556227
# -----------------------------------------------------------
$ EPRNMR_OrbitalShielding
   description: Orbital contribution to the NMR Shielding tensor (ppm)
   geom. index: 1
   prop. index: 1
 Number of stored nuclei 9
 Source density: 1 SCF 
 Nucleus: 4 H 
 Shielding tensor (ppm): 
                  0          1          2    
      0      23.511370  -1.565682   3.250376
      1       3.319165  23.993980   2.387254
      2       2.791272  -1.527053  22.180752
 P Tensor eigenvectors:
                   0          1          2    
      0      -0.632704   0.215904   0.743688
      1       0.053727  -0.945796   0.320287
      2       0.772528   0.242603   0.586809
 P Eigenvalues: 
                  0          1          2    
      0      19.741703  23.625781  26.318618
P(iso)  23.228701
 Nucleus: 5 H 
 Shielding tensor (ppm): 
                  0          1          2    
      0      25.920700   0.642747   2.944471
      1      -0.930832  31.091975  -1.370169
      2       2.551949   0.017045  24.624447
 P Tensor eigenvectors:
                   0          1          2    
      0      -0.620128  -0.776119   0.114372
      1       0.051602  -0.185828  -0.981226
      2       0.782802  -0.602584   0.155286
 P Eigenvalues: 
                  0          1          2    
      0      22.426547  28.006076  31.204498
P(iso)  27.212374
 Nucleus: 6 H 
 Shielding tensor (ppm): 
                  0          1          2    
      0      26.631283   0.192255   0.877530
      1      -1.285951  29.716389   2.835169
      2       0.520604   4.827258  28.011848
 P Tensor eigenvectors:
                   0          1          2    
      0       0.392243   0.919821  -0.008708
      1       0.565736  -0.233762   0.790758
      2      -0.725320   0.315096   0.612067
 P Eigenvalues: 
                  0          1          2    
      0      24.560329  27.012932  32.786260
P(iso)  28.119840
 Nucleus: 7 H 
 Shielding tensor (ppm): 
                  0          1          2    
      0      28.690190   0.086837   3.674060
      1       0.417639  27.577652  -1.099924
      2       3.810150  -2.652384  32.049804
 P Tensor eigenvectors:
                   0          1          2    
      0      -0.652187   0.562776   0.507874
      1       0.538051   0.815601  -0.212830
      2       0.533998  -0.134458   0.834726
 P Eigenvalues: 
                  0          1          2    
      0      25.464488  28.087583  34.765574
P(iso)  29.439215
 Nucleus: 8 H 
 Shielding tensor (ppm): 
                  0          1          2    
      0      33.922680   3.720506  -2.563121
      1       2.942754  26.034754  -0.441346
      2      -1.700881   0.433702  27.436973
 P Tensor eigenvectors:
                   0          1          2    
      0       0.387495   0.123621  -0.913546
      1      -0.875948   0.358249  -0.323069
      2       0.287339   0.925406   0.247105
 P Eigenvalues: 
                  0          1          2    
      0      24.600398  27.160887  35.633122
P(iso)  29.131469
 Nucleus: 0 C 
 Shielding tensor (ppm): 
                  0          1          2    
      0      -3.522912 -33.681261 -75.664021
      1     -24.709333  38.121822 -15.879858
      2     -76.528274 -20.759751  29.267139
 P Tensor eigenvectors:
                   0          1          2    
      0      -0.292593   0.721956  -0.627032
      1       0.886870   0.450076   0.104370
      2      -0.357563   0.525558   0.771970
 P Eigenvalues: 
                  0          1          2    
      0      44.272828 -71.416212  91.009433
P(iso)  21.288683
 Nucleus: 3 C 
 Shielding tensor (ppm): 
                  0          1          2    
      0     166.366278  19.658953   6.692598
      1      15.012735 142.116467  12.084277
      2       7.898519  12.421340 163.147705
 P Tensor eigenvectors:
                   0          1          2    
      0       0.391909   0.591478   0.704671
      1      -0.884945   0.032928   0.464530
      2       0.251556  -0.805648   0.536331
 P Eigenvalues: 
                  0          1          2    
      0     131.130393 157.323077 183.176981
P(iso)  157.210150
 Nucleus: 2 N 
 Shielding tensor (ppm): 
                  0          1          2    
      0     184.333830 -25.751000  21.272794
      1     -58.439732  48.949065 -38.972460
      2      30.404103 -14.082149 184.287284
 P Tensor eigenvectors:
                   0          1          2    
      0       0.187423  -0.650417   0.736091
      1       0.979102   0.063476  -0.193211
      2       0.078944   0.756921   0.648721
 P Eigenvalues: 
                  0          1          2    
      0      35.762497 159.247226 222.560456
P(iso)  139.190060
 Nucleus: 1 O 
 Shielding tensor (ppm): 
                  0          1          2    
      0       6.522582  19.192732 -279.221318
      1       8.176732 -365.575042  64.129559
      2     -288.239740 113.985541  98.005585
 P Tensor eigenvectors:
                   0          1          2    
      0       0.737830  -0.554027  -0.385567
      1       0.351434  -0.172380   0.920206
      2       0.576283   0.814457  -0.067516
 P Eigenvalues: 
                  0          1          2    
      0     -207.165841 289.812930 -343.693964
P(iso)  -87.015625
# -----------------------------------------------------------
$ EPRNMR_SSCoupling
   description: Spin-Spin couplings
   geom. index: 1
   prop. index: 1
 Source density: 0 UNKNOWN 
 Number of nuclei pairs to calculate something:              36
 Number of nuclei pairs to calculate DSO   terms:            36
 Number of nuclei pairs to calculate PSO   terms:            36
 Number of nuclei pairs to calculate FC    terms:            36
 Number of nuclei pairs to calculate SD    terms:            36
 Number of nuclei pairs to calculate PSO   perturbations:    8
 Number of nuclei pairs to calculate SD/FC perturbations:    8
 Information for pairs 
 Pair: 0 Index A: 0 Atomic Number A: 6 Index B: 1 Atomic Number B: 8
          gn_A:  1.4048,  gn_B: -0.7575 Pair Distance: 1.2133
          Total Spin-Spin Coupling ISO:  30.9844 
 Pair: 1 Index A: 0 Atomic Number A: 6 Index B: 2 Atomic Number B: 7
          gn_A:  1.4048,  gn_B: 0.4038 Pair Distance: 1.3537
          Total Spin-Spin Coupling ISO:  10.4652 
 Pair: 2 Index A: 0 Atomic Number A: 6 Index B: 3 Atomic Number B: 6
          gn_A:  1.4048,  gn_B: 1.4048 Pair Distance: 2.4589
          Total Spin-Spin Coupling ISO:  -0.5577 
 Pair: 3 Index A: 0 Atomic Number A: 6 Index B: 4 Atomic Number B: 1
          gn_A:  1.4048,  gn_B: 5.5857 Pair Distance: 1.1045
          Total Spin-Spin Coupling ISO:  184.3127 
 Pair: 4 Index A: 0 Atomic Number A: 6 Index B: 5 Atomic Number B: 1
          gn_A:  1.4048,  gn_B: 5.5857 Pair Distance: 2.0304
          Total Spin-Spin Coupling ISO:  5.6500 
 Pair: 5 Index A: 0 Atomic Number A: 6 Index B: 6 Atomic Number B: 1
          gn_A:  1.4048,  gn_B: 5.5857 Pair Distance: 2.6446
          Total Spin-Spin Coupling ISO:  2.4621 
 Pair: 6 Index A: 0 Atomic Number A: 6 Index B: 7 Atomic Number B: 1
          gn_A:  1.4048,  gn_B: 5.5857 Pair Distance: 3.3152
          Total Spin-Spin Coupling ISO:  6.9397 
 Pair: 7 Index A: 0 Atomic Number A: 6 Index B: 8 Atomic Number B: 1
          gn_A:  1.4048,  gn_B: 5.5857 Pair Distance: 2.9315
          Total Spin-Spin Coupling ISO:  0.2443 
 Pair: 8 Index A: 1 Atomic Number A: 8 Index B: 2 Atomic Number B: 7
          gn_A:  -0.7575,  gn_B: 0.4038 Pair Distance: 2.2764
          Total Spin-Spin Coupling ISO:  0.6486 
 Pair: 9 Index A: 1 Atomic Number A: 8 Index B: 3 Atomic Number B: 6
          gn_A:  -0.7575,  gn_B: 1.4048 Pair Distance: 2.8344
          Total Spin-Spin Coupling ISO:  -0.6856 
 Pair: 10 Index A: 1 Atomic Number A: 8 Index B: 4 Atomic Number B: 1
          gn_A:  -0.7575,  gn_B: 5.5857 Pair Distance: 2.0352
          Total Spin-Spin Coupling ISO:  -6.8626 
 Pair: 11 Index A: 1 Atomic Number A: 8 Index B: 5 Atomic Number B: 1
          gn_A:  -0.7575,  gn_B: 5.5857 Pair Distance: 3.1451
          Total Spin-Spin Coupling ISO:  0.3349 
 Pair: 12 Index A: 1 Atomic Number A: 8 Index B: 6 Atomic Number B: 1
          gn_A:  -0.7575,  gn_B: 5.5857 Pair Distance: 2.6178
          Total Spin-Spin Coupling ISO:  -0.2258 
 Pair: 13 Index A: 1 Atomic Number A: 8 Index B: 7 Atomic Number B: 1
          gn_A:  -0.7575,  gn_B: 5.5857 Pair Distance: 3.8441
          Total Spin-Spin Coupling ISO:  -0.2754 
 Pair: 14 Index A: 1 Atomic Number A: 8 Index B: 8 Atomic Number B: 1
          gn_A:  -0.7575,  gn_B: 5.5857 Pair Distance: 3.1372
          Total Spin-Spin Coupling ISO:  -0.2078 
 Pair: 15 Index A: 2 Atomic Number A: 7 Index B: 3 Atomic Number B: 6
          gn_A:  0.4038,  gn_B: 1.4048 Pair Distance: 1.4519
          Total Spin-Spin Coupling ISO:  7.9361 
 Pair: 16 Index A: 2 Atomic Number A: 7 Index B: 4 Atomic Number B: 1
          gn_A:  0.4038,  gn_B: 5.5857 Pair Distance: 2.0472
          Total Spin-Spin Coupling ISO:  14.2424 
 Pair: 17 Index A: 2 Atomic Number A: 7 Index B: 5 Atomic Number B: 1
          gn_A:  0.4038,  gn_B: 5.5857 Pair Distance: 1.0049
          Total Spin-Spin Coupling ISO:  64.6787 
 Pair: 18 Index A: 2 Atomic Number A: 7 Index B: 6 Atomic Number B: 1
          gn_A:  0.4038,  gn_B: 5.5857 Pair Distance: 2.0824
          Total Spin-Spin Coupling ISO:  -0.6014 
 Pair: 19 Index A: 2 Atomic Number A: 7 Index B: 7 Atomic Number B: 1
          gn_A:  0.4038,  gn_B: 5.5857 Pair Distance: 2.0819
          Total Spin-Spin Coupling ISO:  0.3018 
 Pair: 20 Index A: 2 Atomic Number A: 7 Index B: 8 Atomic Number B: 1
          gn_A:  0.4038,  gn_B: 5.5857 Pair Distance: 2.1160
          Total Spin-Spin Coupling ISO:  -0.4406 
 Pair: 21 Index A: 3 Atomic Number A: 6 Index B: 4 Atomic Number B: 1
          gn_A:  1.4048,  gn_B: 5.5857 Pair Distance: 3.4004
          Total Spin-Spin Coupling ISO:  4.8630 
 Pair: 22 Index A: 3 Atomic Number A: 6 Index B: 5 Atomic Number B: 1
          gn_A:  1.4048,  gn_B: 5.5857 Pair Distance: 2.1334
          Total Spin-Spin Coupling ISO:  4.3085 
 Pair: 23 Index A: 3 Atomic Number A: 6 Index B: 6 Atomic Number B: 1
          gn_A:  1.4048,  gn_B: 5.5857 Pair Distance: 1.0880
          Total Spin-Spin Coupling ISO:  137.5793 
 Pair: 24 Index A: 3 Atomic Number A: 6 Index B: 7 Atomic Number B: 1
          gn_A:  1.4048,  gn_B: 5.5857 Pair Distance: 1.0880
          Total Spin-Spin Coupling ISO:  129.8327 
 Pair: 25 Index A: 3 Atomic Number A: 6 Index B: 8 Atomic Number B: 1
          gn_A:  1.4048,  gn_B: 5.5857 Pair Distance: 1.0922
          Total Spin-Spin Coupling ISO:  136.4323 
 Pair: 26 Index A: 4 Atomic Number A: 1 Index B: 5 Atomic Number B: 1
          gn_A:  5.5857,  gn_B: 5.5857 Pair Distance: 2.2518
          Total Spin-Spin Coupling ISO:  1.2672 
 Pair: 27 Index A: 4 Atomic Number A: 1 Index B: 6 Atomic Number B: 1
          gn_A:  5.5857,  gn_B: 5.5857 Pair Distance: 3.6894
          Total Spin-Spin Coupling ISO:  -1.4022 
 Pair: 28 Index A: 4 Atomic Number A: 1 Index B: 7 Atomic Number B: 1
          gn_A:  5.5857,  gn_B: 5.5857 Pair Distance: 4.1144
          Total Spin-Spin Coupling ISO:  -0.3003 
 Pair: 29 Index A: 4 Atomic Number A: 1 Index B: 8 Atomic Number B: 1
          gn_A:  5.5857,  gn_B: 5.5857 Pair Distance: 3.8777
          Total Spin-Spin Coupling ISO:  -1.5047 
 Pair: 30 Index A: 5 Atomic Number A: 1 Index B: 6 Atomic Number B: 1
          gn_A:  5.5857,  gn_B: 5.5857 Pair Distance: 2.9307
          Total Spin-Spin Coupling ISO:  6.9905 
 Pair: 31 Index A: 5 Atomic Number A: 1 Index B: 7 Atomic Number B: 1
          gn_A:  5.5857,  gn_B: 5.5857 Pair Distance: 2.3444
          Total Spin-Spin Coupling ISO:  5.8627 
 Pair: 32 Index A: 5 Atomic Number A: 1 Index B: 8 Atomic Number B: 1
          gn_A:  5.5857,  gn_B: 5.5857 Pair Distance: 2.7416
          Total Spin-Spin Coupling ISO:  -0.0061 
 Pair: 33 Index A: 6 Atomic Number A: 1 Index B: 7 Atomic Number B: 1
          gn_A:  5.5857,  gn_B: 5.5857 Pair Distance: 1.7744
          Total Spin-Spin Coupling ISO:  -12.1466 
 Pair: 34 Index A: 6 Atomic Number A: 1 Index B: 8 Atomic Number B: 1
          gn_A:  5.5857,  gn_B: 5.5857 Pair Distance: 1.7653
          Total Spin-Spin Coupling ISO:  -11.5759 
 Pair: 35 Index A: 7 Atomic Number A: 1 Index B: 8 Atomic Number B: 1
          gn_A:  5.5857,  gn_B: 5.5857 Pair Distance: 1.7755
          Total Spin-Spin Coupling ISO:  -10.1202 
# -------------------------------------------------------------
----------------------- !GEOMETRIES! ------------------------
# -------------------------------------------------------------
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    9 
    Geometry Index:     1 
    Coordinates:
               0 C      1.342907750022    0.297413365967   -0.313985013642
               1 O      1.280073117944    1.499512923807   -0.466130804269
               2 N      2.230374053894   -0.335566481230    0.488663012719
               3 C      3.238968469388    0.379945203343    1.249419068889
               4 H      0.659503630046   -0.405799095569   -0.822259207659
               5 H      2.217828730815   -1.340173773347    0.507868098698
               6 H      2.805694756495    1.293551157247    1.651131044259
               7 H      3.580706454930   -0.246452198713    2.070740587284
               8 H      4.095443036465    0.653888898495    0.629573213721
