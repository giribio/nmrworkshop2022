-------------------------------------------------------------
----------------------- !PROPERTIES! ------------------------
-------------------------------------------------------------
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 1
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 1
   prop. index: 1
        SCF Energy:     -209.1785805790
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 1
   prop. index: 1
   Number of Alpha Electrons                 16.0000129445 
   Number of Beta  Electrons                 16.0000129445 
   Total number of  Electrons                32.0000258890 
   Exchange energy                          -21.4721351899 
   Correlation energy                        -1.2751878549 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy              -22.7473230448 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)    -209.1785805790 
# -----------------------------------------------------------
$ Mayer_Pop
   description: The Mayer population analysis
   geom. index: 1
   prop. index: 1
     Number of atoms                     : 9
     Threshold for printing              : 0.1000000
     Number of bond orders printed       : 8
  NA   - Mulliken gross atomic population
  ZA   - Total nuclear charge
  QA   - Mulliken gross atomic charge
  VA   - Mayer's total valence
  BVA  - Mayer's bonded valence
  FA   - Mayer's free valence
  ATOM       NA         ZA         QA         VA         BVA        FA
  0   0     5.8301     6.0000     0.1699     4.2231     4.2231    -0.0000
  1   0     8.3570     8.0000    -0.3570     2.1402     2.1402    -0.0000
  2   0     7.1905     7.0000    -0.1905     3.0463     3.0463     0.0000
  3   0     6.1795     6.0000    -0.1795     3.8251     3.8251    -0.0000
  4   0     0.9408     1.0000     0.0592     1.0018     1.0018    -0.0000
  5   0     0.8512     1.0000     0.1488     1.0201     1.0201    -0.0000
  6   0     0.8902     1.0000     0.1098     0.9878     0.9878     0.0000
  7   0     0.8888     1.0000     0.1112     0.9848     0.9848    -0.0000
  8   0     0.8719     1.0000     0.1281     1.0210     1.0210    -0.0000
      Bond orders larger than 0.1000000
           Atom A     A.N. of A          Atom B    A.N. of B              Bond order
                0             6               1            8                2.006709
                0             6               2            7                1.202898
                0             6               4            1                0.981854
                2             7               3            6                0.879266
                2             7               5            1                0.972542
                3             6               6            1                0.977146
                3             6               7            1                0.975708
                3             6               8            1                0.967563
# -----------------------------------------------------------
$ Calculation_Info
   description: Details of the calculation
   geom. index: 1
   prop. index: 1
     Multiplicity:                          1
     Charge:                                0
     number of atoms:                       9
     number of electrons:                   0
     number of frozen core electrons:       1
     number of correlated electrons:        0
     number of basis functions:             194
     number of aux C basis functions:       0
     number of aux J basis functions:       251
     number of aux JK basis functions:      0
     number of aux CABS basis functions:    0
     Total Energy                           -209.178581
# -----------------------------------------------------------
$ SCF_Electric_Properties
   description: The SCF Calculated Electric Properties
   geom. index: 1
   prop. index: 1
       Filename                          : nmr.scfp 
       Do Dipole Moment Calculation      : true 
       Do Quadrupole Moment Calculation  : false 
       Do Polarizability Calculation     : false 
** Dipole moment part of electric properties **
        Magnitude of dipole moment (Debye) :        3.9691539812
        Electronic Contribution:
                  0    
      0      -1.789823
      1       0.567350
      2      -2.737383
        Nuclear Contribution:
                  0    
      0       1.774404
      1      -1.628996
      2       3.882424
        Total Dipole moment:
                  0    
      0      -0.015419
      1      -1.061646
      2       1.145041
# -----------------------------------------------------------
$ EPRNMR_OrbitalShielding
   description: Orbital contribution to the NMR Shielding tensor (ppm)
   geom. index: 1
   prop. index: 1
 Number of stored nuclei 9
 Source density: 1 SCF 
 Nucleus: 4 H 
 Shielding tensor (ppm): 
                  0          1          2    
      0      23.713692   0.511537   4.088877
      1       3.839439  22.884305   1.071440
      2       0.553799  -2.651191  23.242024
 P Tensor eigenvectors:
                   0          1          2    
      0      -0.623385  -0.121524   0.772414
      1       0.566788  -0.750742   0.339319
      2       0.538648   0.649321   0.536879
 P Eigenvalues: 
                  0          1          2    
      0      19.731842  23.848068  26.260110
P(iso)  23.280007
 Nucleus: 5 H 
 Shielding tensor (ppm): 
                  0          1          2    
      0      26.524625   3.544476   0.759234
      1       1.891477  28.205366  -3.775389
      2       1.995815  -2.096073  27.487117
 P Tensor eigenvectors:
                   0          1          2    
      0       0.612110  -0.733491   0.295485
      1      -0.584368  -0.167826   0.793945
      2      -0.532762  -0.658654  -0.531357
 P Eigenvalues: 
                  0          1          2    
      0      22.700933  28.340660  31.175513
P(iso)  27.405702
 Nucleus: 6 H 
 Shielding tensor (ppm): 
                  0          1          2    
      0      27.920408   1.283554   0.767034
      1       0.561309  26.151247   4.208808
      2       0.383593   2.712793  34.417488
 P Tensor eigenvectors:
                   0          1          2    
      0      -0.217842   0.970449   0.103793
      1       0.926331   0.172099   0.335101
      2      -0.307336  -0.169146   0.936448
 P Eigenvalues: 
                  0          1          2    
      0      24.750203  27.971650  35.767290
P(iso)  29.496381
 Nucleus: 7 H 
 Shielding tensor (ppm): 
                  0          1          2    
      0      35.014606  -3.394060   1.345775
      1      -2.285784  27.567179   0.811834
      2       1.524588   1.362632  25.770692
 P Tensor eigenvectors:
                   0          1          2    
      0       0.260024  -0.198448  -0.944990
      1       0.530170  -0.788603   0.311489
      2      -0.807036  -0.582000  -0.099845
 P Eigenvalues: 
                  0          1          2    
      0      24.584924  27.692606  36.074947
P(iso)  29.450826
 Nucleus: 8 H 
 Shielding tensor (ppm): 
                  0          1          2    
      0      26.523242   3.228344   0.836157
      1       1.969951  29.280321  -2.284629
      2       2.203491  -0.502844  26.302863
 P Tensor eigenvectors:
                   0          1          2    
      0      -0.674336   0.568391  -0.471383
      1       0.447355  -0.193423  -0.873190
      2       0.587490   0.799699   0.123841
 P Eigenvalues: 
                  0          1          2    
      0      23.493988  27.726770  30.885668
P(iso)  27.368809
 Nucleus: 0 C 
 Shielding tensor (ppm): 
                  0          1          2    
      0      -6.850518 -44.610632 -66.145943
      1     -37.567884  55.041756  -5.287853
      2     -72.291086 -11.657914  18.539277
 P Tensor eigenvectors:
                   0          1          2    
      0      -0.029505   0.805489  -0.591876
      1      -0.681751   0.416815   0.601232
      2       0.730989   0.421252   0.536845
 P Eigenvalues: 
                  0          1          2    
      0      45.260718 -69.131764  90.601561
P(iso)  22.243505
 Nucleus: 3 C 
 Shielding tensor (ppm): 
                  0          1          2    
      0     176.861742   7.727848   7.119204
      1       4.878827 149.649030  20.809850
      2       9.727010  22.100866 153.129167
 P Tensor eigenvectors:
                   0          1          2    
      0       0.017966   0.637475  -0.770261
      1       0.729704  -0.535024  -0.425771
      2      -0.683527  -0.554413  -0.474781
 P Eigenvalues: 
                  0          1          2    
      0     129.839479 164.252106 185.548354
P(iso)  159.879980
 Nucleus: 2 N 
 Shielding tensor (ppm): 
                  0          1          2    
      0     133.883064 -51.093346  24.907202
      1     -71.538078  82.857827   0.034867
      2      50.261016  21.342811 194.709918
 P Tensor eigenvectors:
                   0          1          2    
      0       0.528661  -0.637165   0.560837
      1       0.811197   0.573797  -0.112769
      2      -0.249954   0.514566   0.820210
 P Eigenvalues: 
                  0          1          2    
      0      37.249031 159.690421 214.511357
P(iso)  137.150270
 Nucleus: 1 O 
 Shielding tensor (ppm): 
                  0          1          2    
      0      17.161998 -182.269505 -190.885955
      1     -211.003688 -98.284444 233.374431
      2     -156.561595 289.614757 -142.244058
 P Tensor eigenvectors:
                   0          1          2    
      0       0.784134  -0.616953   0.067103
      1       0.397537   0.416324  -0.817703
      2       0.476548   0.667865   0.571716
 P Eigenvalues: 
                  0          1          2    
      0     -190.630897 315.642132 -348.377738
P(iso)  -74.455501
# -----------------------------------------------------------
$ EPRNMR_SSCoupling
   description: Spin-Spin couplings
   geom. index: 1
   prop. index: 1
 Source density: 0 UNKNOWN 
 Number of nuclei pairs to calculate something:              36
 Number of nuclei pairs to calculate DSO   terms:            36
 Number of nuclei pairs to calculate PSO   terms:            36
 Number of nuclei pairs to calculate FC    terms:            36
 Number of nuclei pairs to calculate SD    terms:            36
 Number of nuclei pairs to calculate PSO   perturbations:    8
 Number of nuclei pairs to calculate SD/FC perturbations:    8
 Information for pairs 
 Pair: 0 Index A: 0 Atomic Number A: 6 Index B: 1 Atomic Number B: 8
          gn_A:  1.4048,  gn_B: -0.7575 Pair Distance: 1.2130
          Total Spin-Spin Coupling ISO:  30.9994 
 Pair: 1 Index A: 0 Atomic Number A: 6 Index B: 2 Atomic Number B: 7
          gn_A:  1.4048,  gn_B: 0.4038 Pair Distance: 1.3558
          Total Spin-Spin Coupling ISO:  10.3619 
 Pair: 2 Index A: 0 Atomic Number A: 6 Index B: 3 Atomic Number B: 6
          gn_A:  1.4048,  gn_B: 1.4048 Pair Distance: 2.4704
          Total Spin-Spin Coupling ISO:  -0.4616 
 Pair: 3 Index A: 0 Atomic Number A: 6 Index B: 4 Atomic Number B: 1
          gn_A:  1.4048,  gn_B: 5.5857 Pair Distance: 1.1044
          Total Spin-Spin Coupling ISO:  185.4290 
 Pair: 4 Index A: 0 Atomic Number A: 6 Index B: 5 Atomic Number B: 1
          gn_A:  1.4048,  gn_B: 5.5857 Pair Distance: 2.0268
          Total Spin-Spin Coupling ISO:  5.6354 
 Pair: 5 Index A: 0 Atomic Number A: 6 Index B: 6 Atomic Number B: 1
          gn_A:  1.4048,  gn_B: 5.5857 Pair Distance: 3.2252
          Total Spin-Spin Coupling ISO:  3.5659 
 Pair: 6 Index A: 0 Atomic Number A: 6 Index B: 7 Atomic Number B: 1
          gn_A:  1.4048,  gn_B: 5.5857 Pair Distance: 3.1410
          Total Spin-Spin Coupling ISO:  1.6478 
 Pair: 7 Index A: 0 Atomic Number A: 6 Index B: 8 Atomic Number B: 1
          gn_A:  1.4048,  gn_B: 5.5857 Pair Distance: 2.5361
          Total Spin-Spin Coupling ISO:  4.6070 
 Pair: 8 Index A: 1 Atomic Number A: 8 Index B: 2 Atomic Number B: 7
          gn_A:  -0.7575,  gn_B: 0.4038 Pair Distance: 2.2850
          Total Spin-Spin Coupling ISO:  0.7660 
 Pair: 9 Index A: 1 Atomic Number A: 8 Index B: 3 Atomic Number B: 6
          gn_A:  -0.7575,  gn_B: 1.4048 Pair Distance: 2.8662
          Total Spin-Spin Coupling ISO:  -0.7998 
 Pair: 10 Index A: 1 Atomic Number A: 8 Index B: 4 Atomic Number B: 1
          gn_A:  -0.7575,  gn_B: 5.5857 Pair Distance: 2.0326
          Total Spin-Spin Coupling ISO:  -7.0965 
 Pair: 11 Index A: 1 Atomic Number A: 8 Index B: 5 Atomic Number B: 1
          gn_A:  -0.7575,  gn_B: 5.5857 Pair Distance: 3.1474
          Total Spin-Spin Coupling ISO:  0.6586 
 Pair: 12 Index A: 1 Atomic Number A: 8 Index B: 6 Atomic Number B: 1
          gn_A:  -0.7575,  gn_B: 5.5857 Pair Distance: 3.6880
          Total Spin-Spin Coupling ISO:  -0.1341 
 Pair: 13 Index A: 1 Atomic Number A: 8 Index B: 7 Atomic Number B: 1
          gn_A:  -0.7575,  gn_B: 5.5857 Pair Distance: 3.5267
          Total Spin-Spin Coupling ISO:  -0.1381 
 Pair: 14 Index A: 1 Atomic Number A: 8 Index B: 8 Atomic Number B: 1
          gn_A:  -0.7575,  gn_B: 5.5857 Pair Distance: 2.4370
          Total Spin-Spin Coupling ISO:  0.3211 
 Pair: 15 Index A: 2 Atomic Number A: 7 Index B: 3 Atomic Number B: 6
          gn_A:  0.4038,  gn_B: 1.4048 Pair Distance: 1.4508
          Total Spin-Spin Coupling ISO:  7.9786 
 Pair: 16 Index A: 2 Atomic Number A: 7 Index B: 4 Atomic Number B: 1
          gn_A:  0.4038,  gn_B: 5.5857 Pair Distance: 2.0437
          Total Spin-Spin Coupling ISO:  14.3564 
 Pair: 17 Index A: 2 Atomic Number A: 7 Index B: 5 Atomic Number B: 1
          gn_A:  0.4038,  gn_B: 5.5857 Pair Distance: 1.0054
          Total Spin-Spin Coupling ISO:  64.2412 
 Pair: 18 Index A: 2 Atomic Number A: 7 Index B: 6 Atomic Number B: 1
          gn_A:  0.4038,  gn_B: 5.5857 Pair Distance: 2.1004
          Total Spin-Spin Coupling ISO:  0.1274 
 Pair: 19 Index A: 2 Atomic Number A: 7 Index B: 7 Atomic Number B: 1
          gn_A:  0.4038,  gn_B: 5.5857 Pair Distance: 2.1104
          Total Spin-Spin Coupling ISO:  0.0113 
 Pair: 20 Index A: 2 Atomic Number A: 7 Index B: 8 Atomic Number B: 1
          gn_A:  0.4038,  gn_B: 5.5857 Pair Distance: 2.0627
          Total Spin-Spin Coupling ISO:  -0.7476 
 Pair: 21 Index A: 3 Atomic Number A: 6 Index B: 4 Atomic Number B: 1
          gn_A:  1.4048,  gn_B: 5.5857 Pair Distance: 3.4034
          Total Spin-Spin Coupling ISO:  4.7638 
 Pair: 22 Index A: 3 Atomic Number A: 6 Index B: 5 Atomic Number B: 1
          gn_A:  1.4048,  gn_B: 5.5857 Pair Distance: 2.1304
          Total Spin-Spin Coupling ISO:  4.0599 
 Pair: 23 Index A: 3 Atomic Number A: 6 Index B: 6 Atomic Number B: 1
          gn_A:  1.4048,  gn_B: 5.5857 Pair Distance: 1.0904
          Total Spin-Spin Coupling ISO:  132.9347 
 Pair: 24 Index A: 3 Atomic Number A: 6 Index B: 7 Atomic Number B: 1
          gn_A:  1.4048,  gn_B: 5.5857 Pair Distance: 1.0919
          Total Spin-Spin Coupling ISO:  134.3549 
 Pair: 25 Index A: 3 Atomic Number A: 6 Index B: 8 Atomic Number B: 1
          gn_A:  1.4048,  gn_B: 5.5857 Pair Distance: 1.0866
          Total Spin-Spin Coupling ISO:  136.0010 
 Pair: 26 Index A: 4 Atomic Number A: 1 Index B: 5 Atomic Number B: 1
          gn_A:  5.5857,  gn_B: 5.5857 Pair Distance: 2.2366
          Total Spin-Spin Coupling ISO:  1.5596 
 Pair: 27 Index A: 4 Atomic Number A: 1 Index B: 6 Atomic Number B: 1
          gn_A:  5.5857,  gn_B: 5.5857 Pair Distance: 4.0535
          Total Spin-Spin Coupling ISO:  -0.7584 
 Pair: 28 Index A: 4 Atomic Number A: 1 Index B: 7 Atomic Number B: 1
          gn_A:  5.5857,  gn_B: 5.5857 Pair Distance: 4.0083
          Total Spin-Spin Coupling ISO:  -1.0890 
 Pair: 29 Index A: 4 Atomic Number A: 1 Index B: 8 Atomic Number B: 1
          gn_A:  5.5857,  gn_B: 5.5857 Pair Distance: 3.6163
          Total Spin-Spin Coupling ISO:  -1.0899 
 Pair: 30 Index A: 5 Atomic Number A: 1 Index B: 6 Atomic Number B: 1
          gn_A:  5.5857,  gn_B: 5.5857 Pair Distance: 2.4712
          Total Spin-Spin Coupling ISO:  2.4611 
 Pair: 31 Index A: 5 Atomic Number A: 1 Index B: 7 Atomic Number B: 1
          gn_A:  5.5857,  gn_B: 5.5857 Pair Distance: 2.5815
          Total Spin-Spin Coupling ISO:  0.3386 
 Pair: 32 Index A: 5 Atomic Number A: 1 Index B: 8 Atomic Number B: 1
          gn_A:  5.5857,  gn_B: 5.5857 Pair Distance: 2.9678
          Total Spin-Spin Coupling ISO:  10.2676 
 Pair: 33 Index A: 6 Atomic Number A: 1 Index B: 7 Atomic Number B: 1
          gn_A:  5.5857,  gn_B: 5.5857 Pair Distance: 1.7750
          Total Spin-Spin Coupling ISO:  -9.3864 
 Pair: 34 Index A: 6 Atomic Number A: 1 Index B: 8 Atomic Number B: 1
          gn_A:  5.5857,  gn_B: 5.5857 Pair Distance: 1.7746
          Total Spin-Spin Coupling ISO:  -12.2915 
 Pair: 35 Index A: 7 Atomic Number A: 1 Index B: 8 Atomic Number B: 1
          gn_A:  5.5857,  gn_B: 5.5857 Pair Distance: 1.7704
          Total Spin-Spin Coupling ISO:  -12.1403 
# -------------------------------------------------------------
----------------------- !GEOMETRIES! ------------------------
# -------------------------------------------------------------
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    9 
    Geometry Index:     1 
    Coordinates:
               0 C      1.515174740225    0.220101468907   -0.494785097492
               1 O      1.884401881151    1.197253134758   -1.111410351913
               2 N      2.069195618445   -0.243403367483    0.652538456995
               3 C      3.197971972058    0.392412295045    1.305570771719
               4 H      0.663186228423   -0.401663993582   -0.822133994814
               5 H      1.661344550342   -1.063698405603    1.066745791093
               6 H      2.978170148292    0.582196146012    2.356564708427
               7 H      4.096509265794   -0.224332880133    1.238722430259
               8 H      3.385545595269    1.337455602080    0.803207285726
