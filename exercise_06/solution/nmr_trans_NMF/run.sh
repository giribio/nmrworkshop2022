export PATH=/home/Lib/openmpi-4.1.1-install/bin:$PATH
export LD_LIBRARY_PATH=/home/Lib/openmpi-4.1.1-install/lib:$LD_LIBRARY_PATH

export PATH=/home/Lib/ORCA_503:$PATH
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/home/Lib/ORCA_503

# export OMP_NUM_THREADS=1

for f in $(seq -f "%03g" 1 1 36); do
  rm -rf  str_$f
  mkdir str_$f
  cd str_$f
  cp ../../scan_opt_trans_NMF/opt.$f.xyz geom.xyz
  cp ../nmr.com .
  /home/Lib/ORCA_503/orca nmr.com  > nmr.out
  cd ..
done



