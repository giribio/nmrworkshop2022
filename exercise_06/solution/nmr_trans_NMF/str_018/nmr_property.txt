-------------------------------------------------------------
----------------------- !PROPERTIES! ------------------------
-------------------------------------------------------------
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 1
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 1
   prop. index: 1
        SCF Energy:     -209.1785883673
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 1
   prop. index: 1
   Number of Alpha Electrons                 15.9999929595 
   Number of Beta  Electrons                 15.9999929595 
   Total number of  Electrons                31.9999859189 
   Exchange energy                          -21.4722637997 
   Correlation energy                        -1.2752166418 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy              -22.7474804415 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)    -209.1785883673 
# -----------------------------------------------------------
$ Mayer_Pop
   description: The Mayer population analysis
   geom. index: 1
   prop. index: 1
     Number of atoms                     : 9
     Threshold for printing              : 0.1000000
     Number of bond orders printed       : 8
  NA   - Mulliken gross atomic population
  ZA   - Total nuclear charge
  QA   - Mulliken gross atomic charge
  VA   - Mayer's total valence
  BVA  - Mayer's bonded valence
  FA   - Mayer's free valence
  ATOM       NA         ZA         QA         VA         BVA        FA
  0   0     5.8301     6.0000     0.1699     4.2229     4.2229     0.0000
  1   0     8.3571     8.0000    -0.3571     2.1404     2.1404     0.0000
  2   0     7.1903     7.0000    -0.1903     3.0466     3.0466     0.0000
  3   0     6.1796     6.0000    -0.1796     3.8227     3.8227    -0.0000
  4   0     0.9407     1.0000     0.0593     1.0017     1.0017     0.0000
  5   0     0.8514     1.0000     0.1486     1.0201     1.0201     0.0000
  6   0     0.8721     1.0000     0.1279     1.0222     1.0222    -0.0000
  7   0     0.8889     1.0000     0.1111     0.9855     0.9855    -0.0000
  8   0     0.8897     1.0000     0.1103     0.9873     0.9873     0.0000
      Bond orders larger than 0.1000000
           Atom A     A.N. of A          Atom B    A.N. of B              Bond order
                0             6               1            8                2.006492
                0             6               2            7                1.203278
                0             6               4            1                0.981732
                2             7               3            6                0.878529
                2             7               5            1                0.972433
                3             6               6            1                0.967370
                3             6               7            1                0.976172
                3             6               8            1                0.976729
# -----------------------------------------------------------
$ Calculation_Info
   description: Details of the calculation
   geom. index: 1
   prop. index: 1
     Multiplicity:                          1
     Charge:                                0
     number of atoms:                       9
     number of electrons:                   0
     number of frozen core electrons:       1
     number of correlated electrons:        0
     number of basis functions:             194
     number of aux C basis functions:       0
     number of aux J basis functions:       251
     number of aux JK basis functions:      0
     number of aux CABS basis functions:    0
     Total Energy                           -209.178588
# -----------------------------------------------------------
$ SCF_Electric_Properties
   description: The SCF Calculated Electric Properties
   geom. index: 1
   prop. index: 1
       Filename                          : nmr.scfp 
       Do Dipole Moment Calculation      : true 
       Do Quadrupole Moment Calculation  : false 
       Do Polarizability Calculation     : false 
** Dipole moment part of electric properties **
        Magnitude of dipole moment (Debye) :        3.9624652922
        Electronic Contribution:
                  0    
      0      -2.718761
      1       0.835627
      2      -1.704490
        Nuclear Contribution:
                  0    
      0       3.534107
      1      -2.145462
      2       1.927607
        Total Dipole moment:
                  0    
      0       0.815346
      1      -1.309835
      2       0.223117
# -----------------------------------------------------------
$ EPRNMR_OrbitalShielding
   description: Orbital contribution to the NMR Shielding tensor (ppm)
   geom. index: 1
   prop. index: 1
 Number of stored nuclei 9
 Source density: 1 SCF 
 Nucleus: 4 H 
 Shielding tensor (ppm): 
                  0          1          2    
      0      23.649607  -2.213362   2.172166
      1       2.610601  24.079007   3.042242
      2       3.521425  -0.446396  22.117393
 P Tensor eigenvectors:
                   0          1          2    
      0      -0.567578  -0.417097  -0.709849
      1      -0.212565   0.907185  -0.363086
      2       0.795407  -0.055191  -0.603558
 P Eigenvalues: 
                  0          1          2    
      0      19.738252  23.849117  26.258638
P(iso)  23.282002
 Nucleus: 5 H 
 Shielding tensor (ppm): 
                  0          1          2    
      0      26.658061  -0.304646   2.679232
      1      -2.416716  30.711890   0.441631
      2       2.281252   1.976749  24.849960
 P Tensor eigenvectors:
                   0          1          2    
      0      -0.571468   0.778531  -0.259447
      1      -0.216811   0.161685   0.962731
      2       0.791465   0.606421   0.076396
 P Eigenvalues: 
                  0          1          2    
      0      22.720213  28.335610  31.164087
P(iso)  27.406637
 Nucleus: 6 H 
 Shielding tensor (ppm): 
                  0          1          2    
      0      26.392666   1.176925   2.379964
      1      -0.942907  30.470102   0.490152
      2       1.894336   1.839564  25.109129
 P Tensor eigenvectors:
                   0          1          2    
      0      -0.575833   0.807328   0.128984
      1      -0.125414  -0.243125   0.961853
      2       0.807891   0.537691   0.241250
 P Eigenvalues: 
                  0          1          2    
      0      23.411453  27.767552  30.792891
P(iso)  27.323965
 Nucleus: 7 H 
 Shielding tensor (ppm): 
                  0          1          2    
      0      26.819639   1.593155   1.849516
      1       1.119757  25.927801  -0.525242
      2       1.605339  -1.748398  35.659897
 P Tensor eigenvectors:
                   0          1          2    
      0      -0.614044   0.770978   0.168945
      1       0.768200   0.632929  -0.096278
      2       0.181159  -0.070664   0.980912
 P Eigenvalues: 
                  0          1          2    
      0      24.603027  27.743812  36.060498
P(iso)  29.469112
 Nucleus: 8 H 
 Shielding tensor (ppm): 
                  0          1          2    
      0      35.650647   0.581843  -0.869648
      1       1.963233  25.438868   0.731468
      2      -0.662302   1.562988  27.417047
 P Tensor eigenvectors:
                   0          1          2    
      0       0.124619  -0.025968  -0.991865
      1      -0.895664  -0.433066  -0.101194
      2       0.426915  -0.900988   0.077227
 P Eigenvalues: 
                  0          1          2    
      0      24.715080  27.939646  35.851836
P(iso)  29.502187
 Nucleus: 0 C 
 Shielding tensor (ppm): 
                  0          1          2    
      0      -5.804641 -26.031616 -75.786119
      1     -17.523384  42.837646 -25.646909
      2     -73.297724 -32.619549  29.779174
 P Tensor eigenvectors:
                   0          1          2    
      0       0.519392   0.638375  -0.568075
      1      -0.845653   0.479579  -0.234253
      2       0.122896   0.602064   0.788933
 P Eigenvalues: 
                  0          1          2    
      0      45.188041 -69.176033  90.800172
P(iso)  22.270726
 Nucleus: 3 C 
 Shielding tensor (ppm): 
                  0          1          2    
      0     166.246862  21.839282   6.715011
      1      19.414252 142.470067   8.260253
      2       5.517267  11.092148 171.404826
 P Tensor eigenvectors:
                   0          1          2    
      0       0.474409   0.578123   0.663860
      1      -0.869348   0.189072   0.456602
      2       0.138455  -0.793741   0.592288
 P Eigenvalues: 
                  0          1          2    
      0     129.836088 164.638379 185.647288
P(iso)  160.040585
 Nucleus: 2 N 
 Shielding tensor (ppm): 
                  0          1          2    
      0     196.137781  -9.504567  23.607354
      1     -39.028582  54.718876 -56.601288
      2      13.373390 -34.546858 160.142124
 P Tensor eigenvectors:
                   0          1          2    
      0       0.043233  -0.557929   0.828762
      1       0.956482  -0.216489  -0.195637
      2       0.288570   0.801154   0.524290
 P Eigenvalues: 
                  0          1          2    
      0      37.058536 159.822916 214.117330
P(iso)  136.999594
 Nucleus: 1 O 
 Shielding tensor (ppm): 
                  0          1          2    
      0     -70.694239 184.383339 -249.514249
      1     121.069108 -299.051199 -90.246050
      2     -260.530506 -57.655537 148.040337
 P Tensor eigenvectors:
                   0          1          2    
      0       0.669777  -0.632672  -0.388748
      1       0.437739  -0.086484   0.894933
      2       0.599820   0.769575  -0.219020
 P Eigenvalues: 
                  0          1          2    
      0     -189.940804 332.034549 -363.798847
P(iso)  -73.901701
# -----------------------------------------------------------
$ EPRNMR_SSCoupling
   description: Spin-Spin couplings
   geom. index: 1
   prop. index: 1
 Source density: 0 UNKNOWN 
 Number of nuclei pairs to calculate something:              36
 Number of nuclei pairs to calculate DSO   terms:            36
 Number of nuclei pairs to calculate PSO   terms:            36
 Number of nuclei pairs to calculate FC    terms:            36
 Number of nuclei pairs to calculate SD    terms:            36
 Number of nuclei pairs to calculate PSO   perturbations:    8
 Number of nuclei pairs to calculate SD/FC perturbations:    8
 Information for pairs 
 Pair: 0 Index A: 0 Atomic Number A: 6 Index B: 1 Atomic Number B: 8
          gn_A:  1.4048,  gn_B: -0.7575 Pair Distance: 1.2130
          Total Spin-Spin Coupling ISO:  31.0124 
 Pair: 1 Index A: 0 Atomic Number A: 6 Index B: 2 Atomic Number B: 7
          gn_A:  1.4048,  gn_B: 0.4038 Pair Distance: 1.3558
          Total Spin-Spin Coupling ISO:  10.3846 
 Pair: 2 Index A: 0 Atomic Number A: 6 Index B: 3 Atomic Number B: 6
          gn_A:  1.4048,  gn_B: 1.4048 Pair Distance: 2.4694
          Total Spin-Spin Coupling ISO:  -0.4706 
 Pair: 3 Index A: 0 Atomic Number A: 6 Index B: 4 Atomic Number B: 1
          gn_A:  1.4048,  gn_B: 5.5857 Pair Distance: 1.1043
          Total Spin-Spin Coupling ISO:  185.7013 
 Pair: 4 Index A: 0 Atomic Number A: 6 Index B: 5 Atomic Number B: 1
          gn_A:  1.4048,  gn_B: 5.5857 Pair Distance: 2.0275
          Total Spin-Spin Coupling ISO:  5.6616 
 Pair: 5 Index A: 0 Atomic Number A: 6 Index B: 6 Atomic Number B: 1
          gn_A:  1.4048,  gn_B: 5.5857 Pair Distance: 2.5294
          Total Spin-Spin Coupling ISO:  4.6571 
 Pair: 6 Index A: 0 Atomic Number A: 6 Index B: 7 Atomic Number B: 1
          gn_A:  1.4048,  gn_B: 5.5857 Pair Distance: 3.1547
          Total Spin-Spin Coupling ISO:  1.9014 
 Pair: 7 Index A: 0 Atomic Number A: 6 Index B: 8 Atomic Number B: 1
          gn_A:  1.4048,  gn_B: 5.5857 Pair Distance: 3.2122
          Total Spin-Spin Coupling ISO:  3.2496 
 Pair: 8 Index A: 1 Atomic Number A: 8 Index B: 2 Atomic Number B: 7
          gn_A:  -0.7575,  gn_B: 0.4038 Pair Distance: 2.2847
          Total Spin-Spin Coupling ISO:  0.7734 
 Pair: 9 Index A: 1 Atomic Number A: 8 Index B: 3 Atomic Number B: 6
          gn_A:  -0.7575,  gn_B: 1.4048 Pair Distance: 2.8641
          Total Spin-Spin Coupling ISO:  -0.8418 
 Pair: 10 Index A: 1 Atomic Number A: 8 Index B: 4 Atomic Number B: 1
          gn_A:  -0.7575,  gn_B: 5.5857 Pair Distance: 2.0328
          Total Spin-Spin Coupling ISO:  -7.1399 
 Pair: 11 Index A: 1 Atomic Number A: 8 Index B: 5 Atomic Number B: 1
          gn_A:  -0.7575,  gn_B: 5.5857 Pair Distance: 3.1477
          Total Spin-Spin Coupling ISO:  0.6788 
 Pair: 12 Index A: 1 Atomic Number A: 8 Index B: 6 Atomic Number B: 1
          gn_A:  -0.7575,  gn_B: 5.5857 Pair Distance: 2.4254
          Total Spin-Spin Coupling ISO:  0.3250 
 Pair: 13 Index A: 1 Atomic Number A: 8 Index B: 7 Atomic Number B: 1
          gn_A:  -0.7575,  gn_B: 5.5857 Pair Distance: 3.5537
          Total Spin-Spin Coupling ISO:  -0.1325 
 Pair: 14 Index A: 1 Atomic Number A: 8 Index B: 8 Atomic Number B: 1
          gn_A:  -0.7575,  gn_B: 5.5857 Pair Distance: 3.6606
          Total Spin-Spin Coupling ISO:  -0.1370 
 Pair: 15 Index A: 2 Atomic Number A: 7 Index B: 3 Atomic Number B: 6
          gn_A:  0.4038,  gn_B: 1.4048 Pair Distance: 1.4508
          Total Spin-Spin Coupling ISO:  7.9345 
 Pair: 16 Index A: 2 Atomic Number A: 7 Index B: 4 Atomic Number B: 1
          gn_A:  0.4038,  gn_B: 5.5857 Pair Distance: 2.0438
          Total Spin-Spin Coupling ISO:  14.3431 
 Pair: 17 Index A: 2 Atomic Number A: 7 Index B: 5 Atomic Number B: 1
          gn_A:  0.4038,  gn_B: 5.5857 Pair Distance: 1.0054
          Total Spin-Spin Coupling ISO:  64.2150 
 Pair: 18 Index A: 2 Atomic Number A: 7 Index B: 6 Atomic Number B: 1
          gn_A:  0.4038,  gn_B: 5.5857 Pair Distance: 2.0609
          Total Spin-Spin Coupling ISO:  -0.7643 
 Pair: 19 Index A: 2 Atomic Number A: 7 Index B: 7 Atomic Number B: 1
          gn_A:  0.4038,  gn_B: 5.5857 Pair Distance: 2.1094
          Total Spin-Spin Coupling ISO:  0.0030 
 Pair: 20 Index A: 2 Atomic Number A: 7 Index B: 8 Atomic Number B: 1
          gn_A:  0.4038,  gn_B: 5.5857 Pair Distance: 2.1024
          Total Spin-Spin Coupling ISO:  0.1485 
 Pair: 21 Index A: 3 Atomic Number A: 6 Index B: 4 Atomic Number B: 1
          gn_A:  1.4048,  gn_B: 5.5857 Pair Distance: 3.4028
          Total Spin-Spin Coupling ISO:  4.7184 
 Pair: 22 Index A: 3 Atomic Number A: 6 Index B: 5 Atomic Number B: 1
          gn_A:  1.4048,  gn_B: 5.5857 Pair Distance: 2.1305
          Total Spin-Spin Coupling ISO:  3.9956 
 Pair: 23 Index A: 3 Atomic Number A: 6 Index B: 6 Atomic Number B: 1
          gn_A:  1.4048,  gn_B: 5.5857 Pair Distance: 1.0866
          Total Spin-Spin Coupling ISO:  135.8155 
 Pair: 24 Index A: 3 Atomic Number A: 6 Index B: 7 Atomic Number B: 1
          gn_A:  1.4048,  gn_B: 5.5857 Pair Distance: 1.0916
          Total Spin-Spin Coupling ISO:  134.1322 
 Pair: 25 Index A: 3 Atomic Number A: 6 Index B: 8 Atomic Number B: 1
          gn_A:  1.4048,  gn_B: 5.5857 Pair Distance: 1.0906
          Total Spin-Spin Coupling ISO:  133.2336 
 Pair: 26 Index A: 4 Atomic Number A: 1 Index B: 5 Atomic Number B: 1
          gn_A:  5.5857,  gn_B: 5.5857 Pair Distance: 2.2379
          Total Spin-Spin Coupling ISO:  1.5478 
 Pair: 27 Index A: 4 Atomic Number A: 1 Index B: 6 Atomic Number B: 1
          gn_A:  5.5857,  gn_B: 5.5857 Pair Distance: 3.6113
          Total Spin-Spin Coupling ISO:  -1.0744 
 Pair: 28 Index A: 4 Atomic Number A: 1 Index B: 7 Atomic Number B: 1
          gn_A:  5.5857,  gn_B: 5.5857 Pair Distance: 4.0146
          Total Spin-Spin Coupling ISO:  -1.0348 
 Pair: 29 Index A: 4 Atomic Number A: 1 Index B: 8 Atomic Number B: 1
          gn_A:  5.5857,  gn_B: 5.5857 Pair Distance: 4.0483
          Total Spin-Spin Coupling ISO:  -0.8152 
 Pair: 30 Index A: 5 Atomic Number A: 1 Index B: 6 Atomic Number B: 1
          gn_A:  5.5857,  gn_B: 5.5857 Pair Distance: 2.9691
          Total Spin-Spin Coupling ISO:  10.3907 
 Pair: 31 Index A: 5 Atomic Number A: 1 Index B: 7 Atomic Number B: 1
          gn_A:  5.5857,  gn_B: 5.5857 Pair Distance: 2.5582
          Total Spin-Spin Coupling ISO:  0.7002 
 Pair: 32 Index A: 5 Atomic Number A: 1 Index B: 8 Atomic Number B: 1
          gn_A:  5.5857,  gn_B: 5.5857 Pair Distance: 2.4948
          Total Spin-Spin Coupling ISO:  1.9255 
 Pair: 33 Index A: 6 Atomic Number A: 1 Index B: 7 Atomic Number B: 1
          gn_A:  5.5857,  gn_B: 5.5857 Pair Distance: 1.7713
          Total Spin-Spin Coupling ISO:  -12.1736 
 Pair: 34 Index A: 6 Atomic Number A: 1 Index B: 8 Atomic Number B: 1
          gn_A:  5.5857,  gn_B: 5.5857 Pair Distance: 1.7732
          Total Spin-Spin Coupling ISO:  -12.2458 
 Pair: 35 Index A: 7 Atomic Number A: 1 Index B: 8 Atomic Number B: 1
          gn_A:  5.5857,  gn_B: 5.5857 Pair Distance: 1.7760
          Total Spin-Spin Coupling ISO:  -9.2671 
# -------------------------------------------------------------
----------------------- !GEOMETRIES! ------------------------
# -------------------------------------------------------------
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    9 
    Geometry Index:     1 
    Coordinates:
               0 C      1.263859375245    0.291320746830   -0.214665201994
               1 O      0.974330042249    1.463793930066   -0.101065818103
               2 N      2.324436627786   -0.318116744943    0.370141237625
               3 C      3.265396517127    0.375604422226    1.229231360253
               4 H      0.670847627489   -0.410341655551   -0.827452069954
               5 H      2.448323621899   -1.301184257064    0.199664263100
               6 H      3.014066615774    1.432531184943    1.210845691629
               7 H      3.203223170564    0.016683334556    2.258266437148
               8 H      4.287016401867    0.246029038936    0.870054100296
