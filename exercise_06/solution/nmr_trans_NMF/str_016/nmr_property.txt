-------------------------------------------------------------
----------------------- !PROPERTIES! ------------------------
-------------------------------------------------------------
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 1
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 1
   prop. index: 1
        SCF Energy:     -209.1785148993
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 1
   prop. index: 1
   Number of Alpha Electrons                 15.9999966337 
   Number of Beta  Electrons                 15.9999966337 
   Total number of  Electrons                31.9999932675 
   Exchange energy                          -21.4718636782 
   Correlation energy                        -1.2750645757 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy              -22.7469282539 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)    -209.1785148993 
# -----------------------------------------------------------
$ Mayer_Pop
   description: The Mayer population analysis
   geom. index: 1
   prop. index: 1
     Number of atoms                     : 9
     Threshold for printing              : 0.1000000
     Number of bond orders printed       : 8
  NA   - Mulliken gross atomic population
  ZA   - Total nuclear charge
  QA   - Mulliken gross atomic charge
  VA   - Mayer's total valence
  BVA  - Mayer's bonded valence
  FA   - Mayer's free valence
  ATOM       NA         ZA         QA         VA         BVA        FA
  0   0     5.8301     6.0000     0.1699     4.2228     4.2228     0.0000
  1   0     8.3573     8.0000    -0.3573     2.1370     2.1370     0.0000
  2   0     7.1907     7.0000    -0.1907     3.0468     3.0468    -0.0000
  3   0     6.1776     6.0000    -0.1776     3.8512     3.8512    -0.0000
  4   0     0.9404     1.0000     0.0596     1.0025     1.0025     0.0000
  5   0     0.8501     1.0000     0.1499     1.0210     1.0210    -0.0000
  6   0     0.8712     1.0000     0.1288     1.0077     1.0077     0.0000
  7   0     0.8886     1.0000     0.1114     0.9831     0.9831    -0.0000
  8   0     0.8939     1.0000     0.1061     0.9902     0.9902     0.0000
      Bond orders larger than 0.1000000
           Atom A     A.N. of A          Atom B    A.N. of B              Bond order
                0             6               1            8                2.006526
                0             6               2            7                1.200911
                0             6               4            1                0.983620
                2             7               3            6                0.890296
                2             7               5            1                0.973466
                3             6               6            1                0.968180
                3             6               7            1                0.975905
                3             6               8            1                0.977965
# -----------------------------------------------------------
$ Calculation_Info
   description: Details of the calculation
   geom. index: 1
   prop. index: 1
     Multiplicity:                          1
     Charge:                                0
     number of atoms:                       9
     number of electrons:                   0
     number of frozen core electrons:       1
     number of correlated electrons:        0
     number of basis functions:             194
     number of aux C basis functions:       0
     number of aux J basis functions:       251
     number of aux JK basis functions:      0
     number of aux CABS basis functions:    0
     Total Energy                           -209.178515
# -----------------------------------------------------------
$ SCF_Electric_Properties
   description: The SCF Calculated Electric Properties
   geom. index: 1
   prop. index: 1
       Filename                          : nmr.scfp 
       Do Dipole Moment Calculation      : true 
       Do Quadrupole Moment Calculation  : false 
       Do Polarizability Calculation     : false 
** Dipole moment part of electric properties **
        Magnitude of dipole moment (Debye) :        3.9909531972
        Electronic Contribution:
                  0    
      0      -2.832536
      1       0.749109
      2      -1.517819
        Nuclear Contribution:
                  0    
      0       3.768057
      1      -2.009044
      2       1.569518
        Total Dipole moment:
                  0    
      0       0.935520
      1      -1.259935
      2       0.051698
# -----------------------------------------------------------
$ EPRNMR_OrbitalShielding
   description: Orbital contribution to the NMR Shielding tensor (ppm)
   geom. index: 1
   prop. index: 1
 Number of stored nuclei 9
 Source density: 1 SCF 
 Nucleus: 4 H 
 Shielding tensor (ppm): 
                  0          1          2    
      0      23.853924  -2.405345   1.577949
      1       2.367178  23.628789   3.310212
      2       3.852552   0.050776  22.259552
 P Tensor eigenvectors:
                   0          1          2    
      0      -0.516795  -0.470955  -0.714930
      1      -0.342978   0.879042  -0.331137
      2       0.784404   0.074076  -0.615811
 P Eigenvalues: 
                  0          1          2    
      0      19.734064  23.716414  26.291786
P(iso)  23.247422
 Nucleus: 5 H 
 Shielding tensor (ppm): 
                  0          1          2    
      0      26.869680  -0.985045   2.257053
      1      -2.959838  30.080063   1.379355
      2       1.996201   2.662192  24.945177
 P Tensor eigenvectors:
                   0          1          2    
      0      -0.532119   0.772810  -0.345852
      1      -0.346863   0.173651   0.921700
      2       0.772357   0.610418   0.175656
 P Eigenvalues: 
                  0          1          2    
      0      22.549017  28.154819  31.191084
P(iso)  27.298307
 Nucleus: 6 H 
 Shielding tensor (ppm): 
                  0          1          2    
      0      26.724460   2.878817   1.909011
      1       0.531462  31.299511  -0.256877
      2       1.377740   0.645822  25.386374
 P Tensor eigenvectors:
                   0          1          2    
      0      -0.599784   0.732924   0.321064
      1       0.113299  -0.319416   0.940817
      2       0.792100   0.600663   0.108541
 P Eigenvalues: 
                  0          1          2    
      0      24.185305  27.282425  31.942616
P(iso)  27.803449
 Nucleus: 7 H 
 Shielding tensor (ppm): 
                  0          1          2    
      0      26.379130   1.649658   1.051822
      1       0.972335  25.537431  -0.022842
      2       0.138664  -0.242889  35.905577
 P Tensor eigenvectors:
                   0          1          2    
      0      -0.598647   0.799180   0.054157
      1       0.800143   0.599778  -0.006055
      2       0.037321  -0.039709   0.998514
 P Eigenvalues: 
                  0          1          2    
      0      24.563042  27.316715  35.942380
P(iso)  29.274046
 Nucleus: 8 H 
 Shielding tensor (ppm): 
                  0          1          2    
      0      35.042531  -0.942902   0.600256
      1       0.757564  25.968441   1.067660
      2       0.776789   1.538872  27.373946
 P Tensor eigenvectors:
                   0          1          2    
      0       0.055372  -0.068589  -0.996107
      1       0.849609   0.527300   0.010920
      2      -0.524498   0.846906  -0.087472
 P Eigenvalues: 
                  0          1          2    
      0      25.172684  28.109950  35.102284
P(iso)  29.461640
 Nucleus: 0 C 
 Shielding tensor (ppm): 
                  0          1          2    
      0      -8.207086 -21.979295 -74.253232
      1     -15.045818  45.137214 -30.748432
      2     -70.326276 -38.029250  27.985600
 P Tensor eigenvectors:
                   0          1          2    
      0       0.593947   0.620909  -0.511565
      1      -0.804087   0.437679  -0.402345
      2      -0.025918   0.650314   0.759223
 P Eigenvalues: 
                  0          1          2    
      0      44.782201 -70.699320  90.832848
P(iso)  21.638576
 Nucleus: 3 C 
 Shielding tensor (ppm): 
                  0          1          2    
      0     162.455146  20.226748   9.668025
      1      19.226270 143.062591   6.593591
      2       7.163497  10.848454 169.219209
 P Tensor eigenvectors:
                   0          1          2    
      0       0.508763   0.574138   0.641503
      1      -0.856093   0.258704   0.447412
      2       0.090916  -0.776813   0.623134
 P Eigenvalues: 
                  0          1          2    
      0     130.535646 160.086914 184.114386
P(iso)  158.245649
 Nucleus: 2 N 
 Shielding tensor (ppm): 
                  0          1          2    
      0     206.092422  -1.256643  31.559872
      1     -29.724771  61.111888 -64.276326
      2      10.560624 -43.157863 147.645152
 P Tensor eigenvectors:
                   0          1          2    
      0      -0.036218  -0.489355   0.871332
      1       0.921465  -0.353800  -0.160398
      2       0.386769   0.797093   0.463738
 P Eigenvalues: 
                  0          1          2    
      0      36.401620 159.313834 219.134008
P(iso)  138.283154
 Nucleus: 1 O 
 Shielding tensor (ppm): 
                  0          1          2    
      0     -123.246885 222.914074 -207.093008
      1     160.010397 -255.192186 -166.730467
      2     -212.263175 -166.096947 131.594909
 P Tensor eigenvectors:
                   0          1          2    
      0      -0.682244  -0.648689   0.337263
      1      -0.378840  -0.080884  -0.921921
      2      -0.625319   0.756743   0.190567
 P Eigenvalues: 
                  0          1          2    
      0     -201.314448 268.149135 -313.678848
P(iso)  -82.281387
# -----------------------------------------------------------
$ EPRNMR_SSCoupling
   description: Spin-Spin couplings
   geom. index: 1
   prop. index: 1
 Source density: 0 UNKNOWN 
 Number of nuclei pairs to calculate something:              36
 Number of nuclei pairs to calculate DSO   terms:            36
 Number of nuclei pairs to calculate PSO   terms:            36
 Number of nuclei pairs to calculate FC    terms:            36
 Number of nuclei pairs to calculate SD    terms:            36
 Number of nuclei pairs to calculate PSO   perturbations:    8
 Number of nuclei pairs to calculate SD/FC perturbations:    8
 Information for pairs 
 Pair: 0 Index A: 0 Atomic Number A: 6 Index B: 1 Atomic Number B: 8
          gn_A:  1.4048,  gn_B: -0.7575 Pair Distance: 1.2131
          Total Spin-Spin Coupling ISO:  31.0175 
 Pair: 1 Index A: 0 Atomic Number A: 6 Index B: 2 Atomic Number B: 7
          gn_A:  1.4048,  gn_B: 0.4038 Pair Distance: 1.3546
          Total Spin-Spin Coupling ISO:  10.4257 
 Pair: 2 Index A: 0 Atomic Number A: 6 Index B: 3 Atomic Number B: 6
          gn_A:  1.4048,  gn_B: 1.4048 Pair Distance: 2.4640
          Total Spin-Spin Coupling ISO:  -0.5543 
 Pair: 3 Index A: 0 Atomic Number A: 6 Index B: 4 Atomic Number B: 1
          gn_A:  1.4048,  gn_B: 5.5857 Pair Distance: 1.1044
          Total Spin-Spin Coupling ISO:  184.9126 
 Pair: 4 Index A: 0 Atomic Number A: 6 Index B: 5 Atomic Number B: 1
          gn_A:  1.4048,  gn_B: 5.5857 Pair Distance: 2.0287
          Total Spin-Spin Coupling ISO:  5.6402 
 Pair: 5 Index A: 0 Atomic Number A: 6 Index B: 6 Atomic Number B: 1
          gn_A:  1.4048,  gn_B: 5.5857 Pair Distance: 2.5941
          Total Spin-Spin Coupling ISO:  3.3267 
 Pair: 6 Index A: 0 Atomic Number A: 6 Index B: 7 Atomic Number B: 1
          gn_A:  1.4048,  gn_B: 5.5857 Pair Distance: 3.0090
          Total Spin-Spin Coupling ISO:  0.3234 
 Pair: 7 Index A: 0 Atomic Number A: 6 Index B: 8 Atomic Number B: 1
          gn_A:  1.4048,  gn_B: 5.5857 Pair Distance: 3.2944
          Total Spin-Spin Coupling ISO:  6.0347 
 Pair: 8 Index A: 1 Atomic Number A: 8 Index B: 2 Atomic Number B: 7
          gn_A:  -0.7575,  gn_B: 0.4038 Pair Distance: 2.2801
          Total Spin-Spin Coupling ISO:  0.6915 
 Pair: 9 Index A: 1 Atomic Number A: 8 Index B: 3 Atomic Number B: 6
          gn_A:  -0.7575,  gn_B: 1.4048 Pair Distance: 2.8484
          Total Spin-Spin Coupling ISO:  -0.7343 
 Pair: 10 Index A: 1 Atomic Number A: 8 Index B: 4 Atomic Number B: 1
          gn_A:  -0.7575,  gn_B: 5.5857 Pair Distance: 2.0342
          Total Spin-Spin Coupling ISO:  -6.9782 
 Pair: 11 Index A: 1 Atomic Number A: 8 Index B: 5 Atomic Number B: 1
          gn_A:  -0.7575,  gn_B: 5.5857 Pair Distance: 3.1459
          Total Spin-Spin Coupling ISO:  0.4676 
 Pair: 12 Index A: 1 Atomic Number A: 8 Index B: 6 Atomic Number B: 1
          gn_A:  -0.7575,  gn_B: 5.5857 Pair Distance: 2.5328
          Total Spin-Spin Coupling ISO:  -0.0723 
 Pair: 13 Index A: 1 Atomic Number A: 8 Index B: 7 Atomic Number B: 1
          gn_A:  -0.7575,  gn_B: 5.5857 Pair Distance: 3.2824
          Total Spin-Spin Coupling ISO:  -0.1718 
 Pair: 14 Index A: 1 Atomic Number A: 8 Index B: 8 Atomic Number B: 1
          gn_A:  -0.7575,  gn_B: 5.5857 Pair Distance: 3.8093
          Total Spin-Spin Coupling ISO:  -0.2009 
 Pair: 15 Index A: 2 Atomic Number A: 7 Index B: 3 Atomic Number B: 6
          gn_A:  0.4038,  gn_B: 1.4048 Pair Distance: 1.4515
          Total Spin-Spin Coupling ISO:  7.8659 
 Pair: 16 Index A: 2 Atomic Number A: 7 Index B: 4 Atomic Number B: 1
          gn_A:  0.4038,  gn_B: 5.5857 Pair Distance: 2.0454
          Total Spin-Spin Coupling ISO:  14.2985 
 Pair: 17 Index A: 2 Atomic Number A: 7 Index B: 5 Atomic Number B: 1
          gn_A:  0.4038,  gn_B: 5.5857 Pair Distance: 1.0051
          Total Spin-Spin Coupling ISO:  64.4374 
 Pair: 18 Index A: 2 Atomic Number A: 7 Index B: 6 Atomic Number B: 1
          gn_A:  0.4038,  gn_B: 5.5857 Pair Distance: 2.0734
          Total Spin-Spin Coupling ISO:  -0.6742 
 Pair: 19 Index A: 2 Atomic Number A: 7 Index B: 7 Atomic Number B: 1
          gn_A:  0.4038,  gn_B: 5.5857 Pair Distance: 2.1175
          Total Spin-Spin Coupling ISO:  -0.3303 
 Pair: 20 Index A: 2 Atomic Number A: 7 Index B: 8 Atomic Number B: 1
          gn_A:  0.4038,  gn_B: 5.5857 Pair Distance: 2.0870
          Total Spin-Spin Coupling ISO:  0.3091 
 Pair: 21 Index A: 3 Atomic Number A: 6 Index B: 4 Atomic Number B: 1
          gn_A:  1.4048,  gn_B: 5.5857 Pair Distance: 3.4017
          Total Spin-Spin Coupling ISO:  4.8199 
 Pair: 22 Index A: 3 Atomic Number A: 6 Index B: 5 Atomic Number B: 1
          gn_A:  1.4048,  gn_B: 5.5857 Pair Distance: 2.1319
          Total Spin-Spin Coupling ISO:  4.1590 
 Pair: 23 Index A: 3 Atomic Number A: 6 Index B: 6 Atomic Number B: 1
          gn_A:  1.4048,  gn_B: 5.5857 Pair Distance: 1.0872
          Total Spin-Spin Coupling ISO:  136.8413 
 Pair: 24 Index A: 3 Atomic Number A: 6 Index B: 7 Atomic Number B: 1
          gn_A:  1.4048,  gn_B: 5.5857 Pair Distance: 1.0925
          Total Spin-Spin Coupling ISO:  135.8672 
 Pair: 25 Index A: 3 Atomic Number A: 6 Index B: 8 Atomic Number B: 1
          gn_A:  1.4048,  gn_B: 5.5857 Pair Distance: 1.0887
          Total Spin-Spin Coupling ISO:  131.0836 
 Pair: 26 Index A: 4 Atomic Number A: 1 Index B: 5 Atomic Number B: 1
          gn_A:  5.5857,  gn_B: 5.5857 Pair Distance: 2.2449
          Total Spin-Spin Coupling ISO:  1.3899 
 Pair: 27 Index A: 4 Atomic Number A: 1 Index B: 6 Atomic Number B: 1
          gn_A:  5.5857,  gn_B: 5.5857 Pair Distance: 3.6554
          Total Spin-Spin Coupling ISO:  -1.2833 
 Pair: 28 Index A: 4 Atomic Number A: 1 Index B: 7 Atomic Number B: 1
          gn_A:  5.5857,  gn_B: 5.5857 Pair Distance: 3.9260
          Total Spin-Spin Coupling ISO:  -1.4041 
 Pair: 29 Index A: 4 Atomic Number A: 1 Index B: 8 Atomic Number B: 1
          gn_A:  5.5857,  gn_B: 5.5857 Pair Distance: 4.0993
          Total Spin-Spin Coupling ISO:  -0.4222 
 Pair: 30 Index A: 5 Atomic Number A: 1 Index B: 6 Atomic Number B: 1
          gn_A:  5.5857,  gn_B: 5.5857 Pair Distance: 2.9506
          Total Spin-Spin Coupling ISO:  8.5855 
 Pair: 31 Index A: 5 Atomic Number A: 1 Index B: 7 Atomic Number B: 1
          gn_A:  5.5857,  gn_B: 5.5857 Pair Distance: 2.6842
          Total Spin-Spin Coupling ISO:  -0.3568 
 Pair: 32 Index A: 5 Atomic Number A: 1 Index B: 8 Atomic Number B: 1
          gn_A:  5.5857,  gn_B: 5.5857 Pair Distance: 2.3831
          Total Spin-Spin Coupling ISO:  4.7309 
 Pair: 33 Index A: 6 Atomic Number A: 1 Index B: 7 Atomic Number B: 1
          gn_A:  5.5857,  gn_B: 5.5857 Pair Distance: 1.7667
          Total Spin-Spin Coupling ISO:  -11.7704 
 Pair: 34 Index A: 6 Atomic Number A: 1 Index B: 8 Atomic Number B: 1
          gn_A:  5.5857,  gn_B: 5.5857 Pair Distance: 1.7746
          Total Spin-Spin Coupling ISO:  -12.3487 
 Pair: 35 Index A: 7 Atomic Number A: 1 Index B: 8 Atomic Number B: 1
          gn_A:  5.5857,  gn_B: 5.5857 Pair Distance: 1.7756
          Total Spin-Spin Coupling ISO:  -9.7610 
# -------------------------------------------------------------
----------------------- !GEOMETRIES! ------------------------
# -------------------------------------------------------------
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    9 
    Geometry Index:     1 
    Coordinates:
               0 C      1.229814521377    0.268887899728   -0.163401322017
               1 O      0.849596235147    1.390990124277    0.097347854518
               2 N      2.367637229440   -0.297574192158    0.304889042030
               3 C      3.269308247675    0.385590217311    1.214399826018
               4 H      0.671406810409   -0.409350077269   -0.832702567213
               5 H      2.558091188827   -1.250073418505    0.046599465377
               6 H      3.202634435146    1.455338411835    1.032201325710
               7 H      3.012412130892    0.199130181887    2.259757998436
               8 H      4.290599201087    0.053380852894    1.035928377141
