export PATH=/home/Lib/openmpi-4.1.1-install/bin:$PATH
export LD_LIBRARY_PATH=/home/Lib/openmpi-4.1.1-install/lib:$LD_LIBRARY_PATH

export PATH=/home/Lib/ORCA_503:$PATH
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/home/Lib/ORCA_503

runorca () {
 rm -rf $mol
 mkdir $mol
 cd $mol
 cp ../$mol.xyz geom.xyz
 cp ../opt.com .
 /home/Lib/ORCA_503/orca opt.com > opt.out 
 cp opt.xyz geom.xyz
 cp ../nmr.com .
 /home/Lib/ORCA_503/orca nmr.com > nmr.out 
 cd ..
  }

mol="phenanthrene"
runorca

mol="phenanthrene_acetylene"
runorca
