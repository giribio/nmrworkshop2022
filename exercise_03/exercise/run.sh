export PATH=/home/Lib/openmpi-4.1.1-install/bin:$PATH
export LD_LIBRARY_PATH=/home/Lib/openmpi-4.1.1-install/lib:$LD_LIBRARY_PATH

export PATH=/home/Lib/ORCA_503:$PATH
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/home/Lib/ORCA_503


cd b3lyp_opt_cis
/home/Lib/ORCA_503/orca opt.com > opt.out 
cd ..

cd b3lyp_opt_trans
/home/Lib/ORCA_503/orca opt.com > opt.out 
cd ..

cd b3lyp_d3bj_opt_cis
/home/Lib/ORCA_503/orca opt.com > opt.out 
cd ..

cd b3lyp_d3bj_opt_trans
/home/Lib/ORCA_503/orca opt.com > opt.out 
cd ..

