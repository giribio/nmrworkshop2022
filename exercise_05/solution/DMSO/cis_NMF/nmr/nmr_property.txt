-------------------------------------------------------------
----------------------- !PROPERTIES! ------------------------
-------------------------------------------------------------
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 1
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 1
   prop. index: 1
        SCF Energy:     -209.1905729792
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 1
   prop. index: 1
   Number of Alpha Electrons                 15.9999959849 
   Number of Beta  Electrons                 15.9999959849 
   Total number of  Electrons                31.9999919697 
   Exchange energy                          -21.4745531019 
   Correlation energy                        -1.2750598580 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy              -22.7496129599 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)    -209.1905729792 
# -----------------------------------------------------------
$ Solvation_Details
   description: Details concerning solvation
   geom. index: 1
   prop. index: 1
     Epsilon:               47.2000000000
     Refrac:                 1.4790000000
     RSolv:                  1.3000000000
     Surface Type:                      2
     Number of Points:                574
     Surface Area:         364.4950718059
     Dielectric Energy:     -0.0165744688
# -----------------------------------------------------------
$ Mayer_Pop
   description: The Mayer population analysis
   geom. index: 1
   prop. index: 1
     Number of atoms                     : 9
     Threshold for printing              : 0.1000000
     Number of bond orders printed       : 8
  NA   - Mulliken gross atomic population
  ZA   - Total nuclear charge
  QA   - Mulliken gross atomic charge
  VA   - Mayer's total valence
  BVA  - Mayer's bonded valence
  FA   - Mayer's free valence
  ATOM       NA         ZA         QA         VA         BVA        FA
  0   0     5.8290     6.0000     0.1710     4.2351     4.2351    -0.0000
  1   0     8.4476     8.0000    -0.4476     2.0388     2.0388    -0.0000
  2   0     7.1640     7.0000    -0.1640     3.0837     3.0837    -0.0000
  3   0     6.1626     6.0000    -0.1626     3.8559     3.8559     0.0000
  4   0     0.9216     1.0000     0.0784     0.9978     0.9978    -0.0000
  5   0     0.8234     1.0000     0.1766     1.0117     1.0117    -0.0000
  6   0     0.8898     1.0000     0.1102     0.9985     0.9985    -0.0000
  7   0     0.8810     1.0000     0.1190     0.9787     0.9787     0.0000
  8   0     0.8809     1.0000     0.1191     0.9786     0.9786     0.0000
      Bond orders larger than 0.1000000
           Atom A     A.N. of A          Atom B    A.N. of B              Bond order
                0             6               1            8                1.952811
                0             6               2            7                1.258124
                0             6               4            1                0.979281
                2             7               3            6                0.919188
                2             7               5            1                0.933603
                3             6               6            1                0.973926
                3             6               7            1                0.971248
                3             6               8            1                0.971152
# -----------------------------------------------------------
$ Calculation_Info
   description: Details of the calculation
   geom. index: 1
   prop. index: 1
     Multiplicity:                          1
     Charge:                                0
     number of atoms:                       9
     number of electrons:                   0
     number of frozen core electrons:       1
     number of correlated electrons:        0
     number of basis functions:             194
     number of aux C basis functions:       0
     number of aux J basis functions:       251
     number of aux JK basis functions:      0
     number of aux CABS basis functions:    0
     Total Energy                           -209.190573
# -----------------------------------------------------------
$ SCF_Electric_Properties
   description: The SCF Calculated Electric Properties
   geom. index: 1
   prop. index: 1
       Filename                          : nmr.scfp 
       Do Dipole Moment Calculation      : true 
       Do Quadrupole Moment Calculation  : false 
       Do Polarizability Calculation     : false 
** Dipole moment part of electric properties **
        Magnitude of dipole moment (Debye) :        5.4340216229
        Electronic Contribution:
                  0    
      0      -2.766087
      1      -1.114903
      2      -2.740311
        Nuclear Contribution:
                  0    
      0       4.437659
      1       1.615769
      2       3.975402
        Total Dipole moment:
                  0    
      0       1.671572
      1       0.500866
      2       1.235091
# -----------------------------------------------------------
$ EPRNMR_OrbitalShielding
   description: Orbital contribution to the NMR Shielding tensor (ppm)
   geom. index: 1
   prop. index: 1
 Number of stored nuclei 9
 Source density: 1 SCF 
 Nucleus: 4 H  
 Shielding tensor (ppm): 
                  0          1          2    
      0      25.166590   0.785783   1.922775
      1      -0.519022  21.213318   1.264696
      2      -1.307058   1.266789  23.847264
 P Tensor eigenvectors:
                   0          1          2    
      0      -0.001981  -0.371880  -0.928279
      1       0.927648   0.345983  -0.140585
      2      -0.373450   0.861395  -0.344289
 P Eigenvalues: 
                  0          1          2    
      0      20.703659  24.237320  25.286193
P(iso)  23.409057
 Nucleus: 5 H  
 Shielding tensor (ppm): 
                  0          1          2    
      0      28.475157  -2.039145  -5.045668
      1      -0.306009  23.307795   2.197514
      2      -0.762396   2.199460  27.831042
 P Tensor eigenvectors:
                   0          1          2    
      0       0.001336   0.748312  -0.663345
      1      -0.926352   0.250781   0.281037
      2       0.376658   0.614115   0.693535
 P Eigenvalues: 
                  0          1          2    
      0      22.415321  25.475233  31.723440
P(iso)  26.537998
 Nucleus: 6 H  
 Shielding tensor (ppm): 
                  0          1          2    
      0      27.675256   0.018815   0.050487
      1      -0.741544  26.387576   2.264383
      2      -1.817543   2.250835  30.995814
 P Tensor eigenvectors:
                   0          1          2    
      0       0.000259   0.974040  -0.226376
      1       0.925879   0.085296   0.368066
      2      -0.377820   0.209692   0.901821
 P Eigenvalues: 
                  0          1          2    
      0      25.465899  27.471690  32.121058
P(iso)  28.352882
 Nucleus: 7 H  
 Shielding tensor (ppm): 
                  0          1          2    
      0      29.138145   4.705022   1.100187
      1       5.126059  30.459083  -0.502319
      2       1.745159   0.839928  26.880401
 P Tensor eigenvectors:
                   0          1          2    
      0       0.717682   0.206108   0.665170
      1      -0.586010  -0.337268   0.736778
      2      -0.376197   0.918569   0.121270
 P Eigenvalues: 
                  0          1          2    
      0      24.443804  27.134092  34.899733
P(iso)  28.825876
 Nucleus: 8 H  
 Shielding tensor (ppm): 
                  0          1          2    
      0      29.140410  -2.601466   4.080158
      1      -2.449155  28.540127  -1.119452
      2       4.836109  -2.463745  28.791959
 P Tensor eigenvectors:
                   0          1          2    
      0      -0.717461   0.205852   0.665489
      1      -0.156702   0.883161  -0.442122
      2       0.678745   0.421488   0.601376
 P Eigenvalues: 
                  0          1          2    
      0      24.436443  27.131895  34.904158
P(iso)  28.824165
 Nucleus: 0 C  
 Shielding tensor (ppm): 
                  0          1          2    
      0     -13.773768  20.738677  51.562722
      1      16.999857  63.569852 -33.879522
      2      42.249170 -33.750334  -6.649497
 P Tensor eigenvectors:
                   0          1          2    
      0      -0.819665   0.572836   0.002840
      1      -0.215077  -0.312339   0.925303
      2      -0.530934  -0.757828  -0.379217
 P Eigenvalues: 
                  0          1          2    
      0      31.568278 -65.621821  77.200129
P(iso)  14.382195
 Nucleus: 3 C  
 Shielding tensor (ppm): 
                  0          1          2    
      0     148.254218   9.388122  23.031142
      1       9.588702 149.672221   8.537286
      2      23.688319   8.554608 167.241399
 P Tensor eigenvectors:
                   0          1          2    
      0       0.838117   0.000086   0.545490
      1      -0.205226  -0.926479   0.315466
      2      -0.505412   0.376346   0.776481
 P Eigenvalues: 
                  0          1          2    
      0     131.882691 146.201109 187.084038
P(iso)  155.055946
 Nucleus: 2 N  
 Shielding tensor (ppm): 
                  0          1          2    
      0     210.112611 -30.381333 -74.950106
      1      -9.095895 136.938150 -49.251118
      2     -22.856395 -49.489370  35.246279
 P Tensor eigenvectors:
                   0          1          2    
      0       0.361981  -0.000465  -0.932185
      1       0.349402   0.927165   0.135216
      2       0.864227  -0.374653   0.335778
 P Eigenvalues: 
                  0          1          2    
      0       5.167716 156.963184 220.166140
P(iso)  127.432347
 Nucleus: 1 O  
 Shielding tensor (ppm): 
                  0          1          2    
      0     -216.091852 -30.497625 -73.872685
      1     -10.765501 247.307697 -165.008673
      2     -26.544885 -165.593741 -96.150248
 P Tensor eigenvectors:
                   0          1          2    
      0      -0.584622  -0.811293  -0.004531
      1       0.302421  -0.212737  -0.929131
      2       0.752834  -0.544561   0.369723
 P Eigenvalues: 
                  0          1          2    
      0     -129.509761 -249.337216 313.912574
P(iso)  -21.644801
# -------------------------------------------------------------
----------------------- !GEOMETRIES! ------------------------
# -------------------------------------------------------------
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    9 
    Geometry Index:     1 
    Coordinates:
               0 C      1.125849902050   -0.013675352045    0.038501314661
               1 O      0.470774291084   -0.395757835379   -0.906913257792
               2 N      2.480161317948    0.008914552676    0.090753500232
               3 C      3.256238579967    0.469777670485    1.221974784376
               4 H      0.665053406477    0.361943308788    0.970777905905
               5 H      2.957804531649   -0.324724948420   -0.732532869607
               6 H      2.574258306753    0.790291144333    2.008407934196
               7 H      3.888172224953    1.319027338559    0.955079889970
               8 H      3.888587439120   -0.323855878997    1.624610798058
