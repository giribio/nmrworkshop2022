-------------------------------------------------------------
----------------------- !PROPERTIES! ------------------------
-------------------------------------------------------------
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 1
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 1
   prop. index: 1
        SCF Energy:     -209.1912219456
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 1
   prop. index: 1
   Number of Alpha Electrons                 15.9999987671 
   Number of Beta  Electrons                 15.9999987671 
   Total number of  Electrons                31.9999975342 
   Exchange energy                          -21.4756617090 
   Correlation energy                        -1.2755708643 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy              -22.7512325733 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)    -209.1912219456 
# -----------------------------------------------------------
$ Solvation_Details
   description: Details concerning solvation
   geom. index: 1
   prop. index: 1
     Epsilon:               24.3000000000
     Refrac:                 1.3610000000
     RSolv:                  1.3000000000
     Surface Type:                      2
     Number of Points:                573
     Surface Area:         360.3742021631
     Dielectric Energy:     -0.0159302507
# -----------------------------------------------------------
$ Mayer_Pop
   description: The Mayer population analysis
   geom. index: 1
   prop. index: 1
     Number of atoms                     : 9
     Threshold for printing              : 0.1000000
     Number of bond orders printed       : 8
  NA   - Mulliken gross atomic population
  ZA   - Total nuclear charge
  QA   - Mulliken gross atomic charge
  VA   - Mayer's total valence
  BVA  - Mayer's bonded valence
  FA   - Mayer's free valence
  ATOM       NA         ZA         QA         VA         BVA        FA
  0   0     5.8268     6.0000     0.1732     4.1984     4.1984     0.0000
  1   0     8.4546     8.0000    -0.4546     2.0373     2.0373     0.0000
  2   0     7.1873     7.0000    -0.1873     3.0890     3.0890    -0.0000
  3   0     6.1758     6.0000    -0.1758     3.8214     3.8214    -0.0000
  4   0     0.9030     1.0000     0.0970     0.9939     0.9939     0.0000
  5   0     0.8045     1.0000     0.1955     0.9901     0.9901    -0.0000
  6   0     0.8912     1.0000     0.1088     1.0218     1.0218    -0.0000
  7   0     0.8787     1.0000     0.1213     0.9858     0.9858     0.0000
  8   0     0.8782     1.0000     0.1218     0.9859     0.9859     0.0000
      Bond orders larger than 0.1000000
           Atom A     A.N. of A          Atom B    A.N. of B              Bond order
                0             6               1            8                1.920838
                0             6               2            7                1.260179
                0             6               4            1                0.979241
                2             7               3            6                0.878835
                2             7               5            1                0.950892
                3             6               6            1                0.971590
                3             6               7            1                0.973720
                3             6               8            1                0.973789
# -----------------------------------------------------------
$ Calculation_Info
   description: Details of the calculation
   geom. index: 1
   prop. index: 1
     Multiplicity:                          1
     Charge:                                0
     number of atoms:                       9
     number of electrons:                   0
     number of frozen core electrons:       1
     number of correlated electrons:        0
     number of basis functions:             194
     number of aux C basis functions:       0
     number of aux J basis functions:       251
     number of aux JK basis functions:      0
     number of aux CABS basis functions:    0
     Total Energy                           -209.191222
# -----------------------------------------------------------
$ SCF_Electric_Properties
   description: The SCF Calculated Electric Properties
   geom. index: 1
   prop. index: 1
       Filename                          : nmr.scfp 
       Do Dipole Moment Calculation      : true 
       Do Quadrupole Moment Calculation  : false 
       Do Polarizability Calculation     : false 
** Dipole moment part of electric properties **
        Magnitude of dipole moment (Debye) :        5.2224721929
        Electronic Contribution:
                  0    
      0      -2.822451
      1      -0.410952
      2      -1.030774
        Nuclear Contribution:
                  0    
      0       4.570493
      1      -0.010471
      2       0.036649
        Total Dipole moment:
                  0    
      0       1.748042
      1      -0.421424
      2      -0.994125
# -----------------------------------------------------------
$ EPRNMR_OrbitalShielding
   description: Orbital contribution to the NMR Shielding tensor (ppm)
   geom. index: 1
   prop. index: 1
 Number of stored nuclei 9
 Source density: 1 SCF 
 Nucleus: 4 H  
 Shielding tensor (ppm): 
                  0          1          2    
      0      24.964692  -0.559154  -1.306636
      1       1.309501  20.366094   1.998403
      2       3.187184   1.982311  24.326688
 P Tensor eigenvectors:
                   0          1          2    
      0      -0.002755  -0.705848  -0.708358
      1      -0.923213   0.274004  -0.269443
      2       0.384278   0.653223  -0.652403
 P Eigenvalues: 
                  0          1          2    
      0      19.538665  24.047495  26.071313
P(iso)  23.219158
 Nucleus: 5 H  
 Shielding tensor (ppm): 
                  0          1          2    
      0      27.755872  -0.097032  -0.149482
      1      -0.980572  22.624626   2.707151
      2      -2.325621   2.713601  28.036718
 P Tensor eigenvectors:
                   0          1          2    
      0      -0.004092   0.843998   0.536331
      1      -0.924005   0.201883  -0.324742
      2       0.382358   0.496902  -0.779032
 P Eigenvalues: 
                  0          1          2    
      0      21.500589  26.938242  29.978385
P(iso)  26.139072
 Nucleus: 6 H  
 Shielding tensor (ppm): 
                  0          1          2    
      0      28.368077   0.346608   0.812303
      1      -0.660786  24.716049   2.436181
      2      -1.665026   2.464923  29.882928
 P Tensor eigenvectors:
                   0          1          2    
      0       0.002695   0.980589   0.196054
      1      -0.928653   0.075179  -0.363252
      2       0.370940   0.181087  -0.910830
 P Eigenvalues: 
                  0          1          2    
      0      23.738649  28.289254  30.939151
P(iso)  27.655685
 Nucleus: 7 H  
 Shielding tensor (ppm): 
                  0          1          2    
      0      29.273637   4.931303   1.037093
      1       5.684215  31.465691  -0.780871
      2       1.274995   0.504487  27.133459
 P Tensor eigenvectors:
                   0          1          2    
      0      -0.731555   0.235793  -0.639710
      1       0.581798  -0.273268  -0.766052
      2       0.355442   0.932591  -0.062726
 P Eigenvalues: 
                  0          1          2    
      0      24.568734  27.470814  35.833240
P(iso)  29.290929
 Nucleus: 8 H  
 Shielding tensor (ppm): 
                  0          1          2    
      0      29.364838  -2.807205   4.162851
      1      -3.189638  29.441542  -1.430903
      2       4.927431  -2.731215  29.046567
 P Tensor eigenvectors:
                   0          1          2    
      0       0.726186  -0.229268   0.648143
      1       0.177243  -0.848454  -0.498709
      2      -0.664258  -0.477035   0.575500
 P Eigenvalues: 
                  0          1          2    
      0      24.560055  27.490712  35.802180
P(iso)  29.284316
 Nucleus: 0 C  
 Shielding tensor (ppm): 
                  0          1          2    
      0     -11.029517 -21.400665 -52.155350
      1     -16.657101  75.893650 -43.524672
      2     -40.924110 -43.519394 -10.572070
 P Tensor eigenvectors:
                   0          1          2    
      0      -0.852736   0.522329  -0.003806
      1       0.204583   0.327272  -0.922518
      2       0.480612   0.787443   0.385936
 P Eigenvalues: 
                  0          1          2    
      0      28.942222 -68.657414  94.007255
P(iso)  18.097354
 Nucleus: 3 C  
 Shielding tensor (ppm): 
                  0          1          2    
      0     146.330508  10.151413  24.705395
      1       9.048813 166.007758   1.172257
      2      22.243539   1.108125 168.557766
 P Tensor eigenvectors:
                   0          1          2    
      0       0.841334  -0.002693   0.540509
      1      -0.205557  -0.926446   0.315347
      2      -0.499903   0.376417   0.780004
 P Eigenvalues: 
                  0          1          2    
      0     129.884362 165.573999 185.437671
P(iso)  160.298677
 Nucleus: 2 N  
 Shielding tensor (ppm): 
                  0          1          2    
      0     196.139890  25.883771  62.371740
      1       7.120536 144.727177 -51.596741
      2      17.715966 -51.127040  42.726137
 P Tensor eigenvectors:
                   0          1          2    
      0      -0.333123   0.003728  -0.942876
      1       0.363051   0.923398  -0.124617
      2       0.870186  -0.383825  -0.308959
 P Eigenvalues: 
                  0          1          2    
      0      13.534980 166.107658 203.950566
P(iso)  127.864401
 Nucleus: 1 O  
 Shielding tensor (ppm): 
                  0          1          2    
      0     -260.476226  40.862222  94.719132
      1      18.112387 283.401475 -188.659449
      2      36.010265 -187.201360 -87.118706
 P Tensor eigenvectors:
                   0          1          2    
      0       0.536884   0.843482   0.017106
      1       0.324209  -0.224997   0.918839
      2       0.778873  -0.487764  -0.394262
 P Eigenvalues: 
                  0          1          2    
      0     -128.121985 -297.925034 361.853561
P(iso)  -21.397819
# -------------------------------------------------------------
----------------------- !GEOMETRIES! ------------------------
# -------------------------------------------------------------
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    9 
    Geometry Index:     1 
    Coordinates:
               0 C      1.134570348317   -0.010770749633    0.040048068963
               1 O      0.398184176344    0.362219255998    0.928644933848
               2 N      2.489928064176    0.002378876801    0.080403055866
               3 C      3.249811062310    0.471181644254    1.224158193586
               4 H      0.754590163704   -0.408861237495   -0.917279625040
               5 H      2.981280520439   -0.334134245321   -0.729657107788
               6 H      2.541952528046    0.774840715550    1.990436890312
               7 H      3.873187089015    1.326373291616    0.958227019753
               8 H      3.886206047650   -0.320547551769    1.622548570501
